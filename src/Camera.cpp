#include "Camera.hpp"

Camera::Camera() {
	if (not device_.open(0)) {
		throw "Opening camera device failed";
	}
}

cv::Mat Camera::grab_frame() {
	cv::Mat frame;

	if (not device_.grab()) {
		throw "Frame grab error";
	}

	if (not device_.retrieve(frame)) {
		throw "Frame retrieve error";
	}

	return frame;
}
