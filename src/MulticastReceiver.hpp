#ifndef NETWORKCAMERA_MULTICASTRECEIVER_HPP
#define NETWORKCAMERA_MULTICASTRECEIVER_HPP

#include <asio/io_service.hpp>
#include <asio/ip/address.hpp>
#include <asio/ip/multicast.hpp>
#include <asio/ip/udp.hpp>
#include <asio/use_future.hpp>
#include <future>
#include <iostream>

class MulticastReceiver {
public:
	MulticastReceiver(const std::string &multicast_address, asio::io_service &service);
	std::pair<asio::ip::address, std::uint16_t> get_connection_data();

private:
	asio::ip::udp::socket _socket;
};

#endif // NETWORKCAMERA_MULTICASTRECEIVER_HPP
