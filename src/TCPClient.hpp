#ifndef TCPCLIENT_HPP
#define TCPCLIENT_HPP

#include <cstdint>
#include <deque>
#include <iostream>
#include <vector>

#include <asio/io_service.hpp>
#include <asio/ip/tcp.hpp>
#include <asio/streambuf.hpp>
#include <asio/use_future.hpp>
#include <asio/write.hpp>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>

using asio::ip::tcp;

class TCPClient {
public:
	TCPClient(const asio::ip::address &address, std::uint16_t port, asio::io_service &io_service);
	void send_frame(cv::Mat &&frame);

private:
	tcp::socket _socket;
	tcp::endpoint _endpoint;
	asio::io_service &_io_service;
	std::deque<std::vector<std::uint8_t>> _data_queue;

	std::vector<std::uint8_t> _prepare_buffer(const std::vector<std::uint8_t> &data);
	void _write();
};

#endif // TCPCLIENT_HPP
