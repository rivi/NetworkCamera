#ifndef NETWORKCAMERA_CAMERA_HPP
#define NETWORKCAMERA_CAMERA_HPP

#include <opencv2/highgui.hpp>

#include "TCPClient.hpp"

class Camera {
public:
	Camera();
	cv::Mat grab_frame();

private:
	cv::VideoCapture device_;
};

#endif // NETWORKCAMERA_CAMERA_HPP
