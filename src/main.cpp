#define ASIO_STANDALONE

#include <asio/io_service.hpp>
#include <asio/signal_set.hpp>

#include <opencv2/core.hpp>

#include "Camera.hpp"
#include "MulticastReceiver.hpp"
#include "TCPClient.hpp"

int main(int /*argc*/, char * /*argv*/ []) {
	asio::io_service io_service;
	bool running = true;

	asio::signal_set signals(io_service, SIGINT, SIGTERM);
	signals.async_wait([&io_service, &running](const asio::error_code & /*error_code*/, int /*signal_number*/) {
		running = false;
		io_service.stop();
	});

	asio::io_service::work work(io_service);
	std::thread thread([&io_service] { io_service.run(); });

	MulticastReceiver receiver("225.0.0.1", io_service);
	asio::ip::address address;
	std::uint16_t port;
	std::tie(address, port) = receiver.get_connection_data();

	TCPClient client(address, port, io_service);
	Camera camera;
	while (running) {
		cv::Mat frame = camera.grab_frame();
		client.send_frame(std::move(frame));
	}

	thread.join();
	return 0;
}
