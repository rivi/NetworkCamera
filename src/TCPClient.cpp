#include "TCPClient.hpp"

TCPClient::TCPClient(const asio::ip::address &address, const std::uint16_t port, asio::io_service &io_service)
    : _socket(io_service, tcp::v4()), _endpoint(address, port), _io_service(io_service) {
  _socket.connect(_endpoint);
}

void TCPClient::send_frame(cv::Mat &&frame) {
	_io_service.post([ this, frame = std::forward<const cv::Mat>(frame) ]() {
		bool write_in_progress = not _data_queue.empty();
		std::vector<std::uint8_t> encoded_image;
		cv::imencode(".jpeg", frame, encoded_image);

		_data_queue.emplace_back(_prepare_buffer(encoded_image));

		if (not write_in_progress) {
			_write();
		}
	});
}

std::vector<std::uint8_t> TCPClient::_prepare_buffer(const std::vector<std::uint8_t> &data) {
	auto data_size = static_cast<std::uint32_t>(data.size());
	std::vector<std::uint8_t> buffer;
	buffer.reserve(data_size + 4u);

	buffer.resize(4);
	for (std::uint8_t i = 0; i < 4; i++) {
		buffer[3 - i] = static_cast<std::uint8_t>(data_size >> (i * 8));
	}

	buffer.insert(std::end(buffer), std::begin(data), std::end(data));

	return buffer;
}

void TCPClient::_write() {
	asio::async_write(_socket, asio::buffer(_data_queue.front(), _data_queue.front().size()),
	                  [this](std::error_code error_code, std::size_t) {
		                  if (not error_code) {
												std::cout << "Wrote" << std::endl;

												_data_queue.pop_front();
												if (not _data_queue.empty()) {
													_write();
												}
											}
											else {
												std::cout << "Closing socket: " << error_code << std::endl;
												_socket.close();
											}
	                  });
}
