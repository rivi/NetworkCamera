#include "MulticastReceiver.hpp"

MulticastReceiver::MulticastReceiver(const std::string &multicast_address, asio::io_service &service)
    : _socket(service) {
  auto address = asio::ip::address::from_string(multicast_address);

	asio::ip::udp::endpoint listen_endpoint(asio::ip::address_v4::any(), 45454);
	_socket.open(listen_endpoint.protocol());
	_socket.set_option(asio::ip::udp::socket::reuse_address(true));
	_socket.bind(listen_endpoint);

	_socket.set_option(asio::ip::multicast::join_group(address.to_v4()));
}

std::pair<asio::ip::address, std::uint16_t> MulticastReceiver::get_connection_data() {
	char data[2];

	asio::ip::udp::endpoint sender_endpoint;
	std::future<std::size_t> future = _socket.async_receive_from(asio::buffer(data), sender_endpoint, asio::use_future);

	if (future.get() != 2) {
		throw std::exception();
	}

	return {sender_endpoint.address(), *reinterpret_cast<std::uint16_t *>(data)};
}
